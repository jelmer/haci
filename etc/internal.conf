# This config is for internal use only.
# Only touch if you know what you are doing!

<static>
	<db>
		dbType	= mysql
	</db>

	<path>
		workdir			= $workdir
		templateIncludePath	= $workdir/Template
		spoolPath		= $workdir/spool
		templateCompilePath	= ${spoolPath}
		authModules		= $workdir/modules/HaCi/Authentication
		plugins			= $workdir/modules/HaCi/Plugins
	 	localePath		= $workdir/locale
		statusFile		= ${spoolPath}/status
		cacheFile		= ${spoolPath}/cache
		tableHashFile		= ${spoolPath}/tableHash
		HaCid			= $workdir/bin/HaCid.pl
		templateInit		= HaCi.tmpl
		imagePathRel		= /Images
		configFile		= HaCi.conf
		whois			= /usr/bin/whois
		imagePath		= ${workdir}${imagePathRel}
		HaCidPID		= ${spoolPath}/HaCid.pid
		lockPath		= ${spoolPath}
	</path>

	<gui>
		titleShort		= HaCi
		titleLong		= HaCi - IP Address Administration
		version			= 0.98c
		jsSrcDir		= /
		jsSrc			= /HaCi.js
		imagePathRel		= /Images
		logo			= ${imagePathRel}/logo.png
		style			= bright
		maxSubnetSize		= 8	
		enableLocaleSupport	= 1
	
		<locales>
			ID	= C
			name	= English
		</locales>
	
		<locales>
			ID	= de_DE
			name	= Deutsch
		</locales>

		<locales>
			ID	= it_IT
			name	= Italiano
		</locales>

		<layouts>
			ID	= bright
			name	= bright
			file	= /HaCi.css
			descr	= Bright Layout
		</layouts>
		<layouts>
			ID	= black
			name	= black
			file	= /HaCi_black.css
			descr	= Black Layout
		</layouts>
	</gui>

	<misc>
		debug			= 0
		dbiTrace		= 0
		ripeDB			= 193.0.6.135
		sessionTimeout		= +4h
		maxMemConsumption       = 100000
		disableCache		= 0
		searchLimit		= 2000

		<networkStates>
			name	= UNSPECIFIED
			ID	= 0
		</networkStates>
		<networkStates>
			name		= ALLOCATED PA
			ID		= 3
			minSize		= 21
			banParents	= 1, 2, 16, 5
		</networkStates>
		<networkStates>
			name		= ALLOCATED PI
			ID		= 4
			minSize		= 21
			banParents	= 1, 2, 16, 5
		</networkStates>
		<networkStates>
			name		= ALLOCATED UNSPECIFIED
			ID		= 17
			banParents	= 1, 2, 16, 5
		</networkStates>
		<networkStates>
			name		= SUB-ALLOCATED PA
			ID		= 15
			parents		= 3
			minSize		= 24
			banParents	= 1, 2, 16
		</networkStates>
		<networkStates>
			name		= LIR-PARTITIONED PA
			ID		= 6
			banParents	= 1, 2, 16
		</networkStates>
		<networkStates>
			name		= LIR-PARTITIONED PI
			ID		= 10
			banParents	= 1, 2, 16
		</networkStates>
		<networkStates>
			name		= EARLY REGISTRATION
			ID		= 5
			banParents	= 1, 2, 16
		</networkStates>
		<networkStates>
			name	= NOT-SET
			ID	= 11
		</networkStates>
		<networkStates>
			name	= ASSIGNED PA
			ID	= 1
			banish	= 1, 2, 16
			parents	= 3, 17, 15, 6, 5
		</networkStates>
		<networkStates>
			name	= ASSIGNED PI
			ID	= 2
			banish	= 1, 2, 16, 5
			parents	= 4, 17, 10
		</networkStates>
		<networkStates>
			name	= ASSIGNED ANYCAST
			ID	= 16
			banish	= 1, 2, 16
			parents	= 3, 4, 17, 15, 10, 6
		</networkStates>
		<networkStates>
			name		= ALLOCATED-BY-RIR
			ID		= 12
			minSize		= 32
			banParents	= 14
			ipv6		= 1
		</networkStates>
		<networkStates>
			name		= ALLOCATED-BY-LIR
			ID		= 13
			parents		= 12, 13
			banParents	= 14
			ipv6		= 1
		</networkStates>
		<networkStates>
			name	= ASSIGNED
			ID	= 14
			minSize	= 48
			parents	= 12, 13
			banish	= 14
			ipv6	= 1
		</networkStates>
		<networkStates>
			name	= IN USE
			# parents	= 1, 2, 16, 14
			ID	= 18
		</networkStates>
		<networkStates>
			name	= RESERVED
			ID	= 7
		</networkStates>
		<networkStates>
			name	= LOCKED
			ID	= 8
		</networkStates>
		<networkStates>
			name	= FREE
			ID	= 9
		</networkStates>
	</misc>
	
	<rights>
		<0>
			long	= User Management
			short	= userMgmt
			order	= 0
		</0>
		<1>
			long	= Group Management
			short	= groupMgmt
			order	= 1
		</1>
		<2>
			long	= Template Management
			short	= tmplMgmt
			order	= 2
		</2>
		<3>
			long	= Show Roots
			short	= showRoots
			order	= 4
		</3>
		<4>
			long	= Show Root Details
			short	= showRootDet
			order	= 5
		</4>
		<5>
			long	= Add Root
			short	= addRoot
			order	= 6
		</5>
		<6>
			long	= Edit Root
			short	= editRoot
			order	= 7
		</6>
		<7>
			long	= Show Networks
			short	= showNets
			order	= 8
		</7>
		<8>
			long	= Show Network Details
			short	= showNetDet
			order	= 9
		</8>
		<9>
			long	= Add Network
			short	= addNet
			order	= 10
		</9>
		<10>
			long	= Edit Network
			short	= editNet
			order	= 11
		</10>
		<11>
			long	= Edit Tree
			short	= editTree
			order	= 12
		</11>
		<12>
			long	= Import ASN Routes
			short	= impASNRoutes
			order	= 13
		</12>
		<13>
			long	= Import DNS Zonefile
			short	= impDNS
			order	= 14
		</13>
		<14>
			long	= Import Config
			short	= impConfig
			order	= 15
		</14>
		<15>
			long	= SearchCompare
			short	= search
			order	= 16
		</15>
		<16>
			long	= Plugin Management
			short	= pluginMgmt
			order	= 3
		</16>
		<17>
			long	= Show audit logs
			short	= showAuditLogs
			order	= 17
		</17>
	</rights>

	<pluginDefaultOndemandMenu>
		type	= label
		value	= Defaults for mode 'on Demand'
	</pluginDefaultOndemandMenu>
	
	<pluginDefaultRecurrentMenu>
		type	= label
		value	= Defaults for mode 'recurrent'
	</pluginDefaultRecurrentMenu>

	<pluginDefaultRecurrentMenu>
		name	= def_recurrent_withSubnets
		descr	= With Subnetworks
		type	= checkbox
		checked	= 0
		help	= Work also with Subnetworks
	</pluginDefaultRecurrentMenu>

	<pluginDefaultRecurrentMenu>
		name	= def_recurrent_onlyHosts
		descr	= Only IP addresses
		type	= checkbox
		checked	= 0
		help	= Work only with IP addresses
	</pluginDefaultRecurrentMenu>
	
	<pluginDefaultRecurrentMenu>
		name		= def_recurrent_maxdepth
		descr		= Max Depth
		type		= textbox
		size		= 1
		maxLength	= 10
		help		= Max subnet depth
		value		= 1
	</pluginDefaultRecurrentMenu>
	
	<pluginDefaultRecurrentMenu>
		name	= def_recurrent_resetLastRun
		descr	= Reset last run
		type	= checkbox
		checked	= 0
		help	= Reset last run so that the Plugin will be run as soon as possible
		nodb	= 1
	</pluginDefaultRecurrentMenu>
	
	<pluginDefaultGlobOndemandMenu>
		value	= Defaults for mode 'on Demand'
		type	= label
	</pluginDefaultGlobOndemandMenu>
	
	<pluginDefaultGlobRecurrentMenu>
		value	= Defaults for mode 'recurrent'
		type	= label
	</pluginDefaultGlobRecurrentMenu>

	<pluginDefaultGlobRecurrentMenu>
		name		= def_glob_recurrent_interval
		descr		= Interval (sec)
		type		= textbox
		size		= 3
		maxLength	= 10
		help		= Time between 2 runs
		value		= 300
	</pluginDefaultGlobRecurrentMenu>
	
	<pluginDefaultGlobRecurrentMenu>
		name	= def_glob_recurrent_resetLastRun
		descr	= Reset last run
		type	= checkbox
		checked	= 0
		help	= Reset last run so that the Plugin will be run as soon as possible
		nodb	= 1
	</pluginDefaultGlobRecurrentMenu>
	
	<pluginDefaultGlobRecurrentMenu>
		type	= hline
	</pluginDefaultGlobRecurrentMenu>
</static>
	
<var>
	thisScript	= ${SCRIPT_NAME}
	showSubs	= 0
	status		= 
</var>

# vim:ts=8
