domain "HaCi"

#: login
msgid "Authentication"
msgstr "Benutzeranmeldung"

msgid "Language"
msgstr "Sprache"

msgid "Username"
msgstr "Benutzername"

msgid "Password"
msgstr "Passwort"

msgid "Authentication failed!"
msgstr "Authentifizierung schlug fehl!"

msgid "Authentication failed: User not found!"
msgstr "Authentifizierung schlug fehl: Benutzername wurde nicht gefunden!"

msgid "No User given"
msgstr "Keinen Usernamen angegeben"

msgid "Session is expired"
msgstr "Ihre Sitzung ist abgelaufen"

msgid "Authentication failed: Database Error!"
msgstr "Authentifizierung schlug fehl: Datenbank Fehler"

msgid "Authentication failed: Password doesn't match!"
msgstr "Authentifizierung schlug fehl: Passwort stimmt nicht!"

msgid "Support"
msgstr "Unterst�tzt"

#: misc
msgid "or"
msgstr "oder"

msgid "NEW"
msgstr "NEU"

msgid "Users"
msgstr "Benutzer"

msgid "New User"
msgstr "Neuer Benutzer"

msgid "New Group"
msgstr "Neue Gruppe"

msgid "New Template"
msgstr "Neue Vorlage"

msgid "Login"
msgstr "Anmelden"

msgid "Save result under"
msgstr "Ergebnis speichern unter"

msgid "Right"
msgstr "Rechts"

msgid "Left"
msgstr "Links"

msgid "Compare"
msgstr "Vergleichen"

msgid "Target"
msgstr "Ziel"

msgid "Search,Compare"
msgstr "Suchen, Vergleichen"

msgid "Import ASN routes"
msgstr "ASN Routen importieren"

msgid "Import DNS zonefile"
msgstr "DNS Zonefile importieren"

msgid "No IP Addresses found for Origin '%s'."
msgstr "Keine IP Adressen f�r Origin '%s' gefunden."

msgid "Zone transfer failed: %s"
msgstr "Zonefile transfer fehlgeschlagen: %s"

msgid "Import Config"
msgstr "Konfig importieren"

msgid "No File given!"
msgstr "Keine Datei mitgegeben!"

msgid "Cannot open File in Binmode: %s"
msgstr "Konnte Datei nicht im Bin�r-Modus �ffnen: %s"

msgid "%i IP Addresses found for Origin '%s'. %i were successfully saved under Root '%s'"
msgstr "%i IP Adressen gefunden f�r Origin '%s'. %i wurden erfolgreich unter dem Startpunkt '%s' gespeichert."

msgid "No Origin given or found!"
msgstr "Kein Origin angegeben oder gefunden"

msgid "Edit Network"
msgstr "Netz bearbeiten"

msgid "Show Root Details"
msgstr "Startpunkt Details anzeigen"

msgid "Show Roots"
msgstr "Startpunke anzeigen"

msgid "Show Networks"
msgstr "Netze anzeigen"

msgid "Show Network Details"
msgstr "Netz Details anzeigen"

msgid "Edit Root"
msgstr "Startpunkt bearbeiten"

msgid "Root Name"
msgstr "Startpunkt"

msgid "Cannot read ConfigFile '%s': %s"
msgstr "Kann die Konfigurations Datei '%s' nicht zum lesen �ffnen: %s"

msgid "Abort"
msgstr "Abbrechen"

msgid "Submit"
msgstr "Speichern"

msgid "Change"
msgstr "�ndern"

msgid "New"
msgstr "Neu"

msgid "Delete"
msgstr "L�schen"

msgid "Combine"
msgstr "Zusammenfassen"

msgid "DNS Info for %s"
msgstr "DNS Info f�r %s"

msgid "RIPE Info for %s"
msgstr "RIPE Info f�r %s"

msgid "unknown"
msgstr ""

msgid "Nothing found"
msgstr "Nichts gefunden"

msgid "Error while deleting '%s' from '%s': %s"
msgstr "Fehler beim L�schen '%s' von '%s': %s"

msgid "Successfully deleted '%s' from '%s' (%i Networks deleted)"
msgstr "Erfolgreich '%s' von '%s' gel�scht (%i Netze gel�scht)"

msgid "Successfully deleted '%s' from '%s' (%i Networks deleted) and locked for %i seconds."
msgstr "Erfolgreich '%s' von '%s' gel�scht (%i Netze gel�scht) und f�r %i Sekunden gesperrt"

msgid "None"
msgstr "Keine"

msgid "invisible"
msgstr "unsichtbar"

msgid "read"
msgstr "lesen"

msgid "write"
msgstr "schreiben"

msgid "Group"
msgstr "Gruppe"

msgid "CSV Preview"
msgstr "CSV Vorschau"

msgid "Cannot parse any CSV Data"
msgstr "Konnte keine CSV Daten parsen"

msgid "[ permission denied ]"
msgstr "[ Zugriff verweigert ]"

msgid "This Network is locked"
msgstr "Dieses Netz ist gesperrt"

msgid "This Network is reserved"
msgstr "Dieses Netz ist reserviert"

msgid "This Network is free"
msgstr "Dieses Netz ist frei"

msgid "Contact"
msgstr "Kontakt"

msgid "Successfully updated Table '%s'."
msgstr "Die Tabelle '%s' wurde erfolgreich aktualisiert."

msgid "Cannot update Table '%s': %s. Please correct this issue by hand (i.e.: remove duplicate entries)!"
msgstr "Konnte Tabelle '%s' nicht aktualisieren: %s. Bitte korrigieren Sie diesen Fehler per Hand (z.B.: doppelte Eintr�ge l�schen)!"

msgid "Cannot upgrade tables automatically. Required modules are not available (%s)."
msgstr "Keine automatische Aktualisierung der Tabellen m�glich. Ben�tigte Module sind nicht verf�gbar (%s)."

msgid "Automatic upgrade of database disabled. Please upgrade the database schema by hand."
msgstr "Automatische Aktualisierung der Datenbank nicht aktiviert. Bitte aktualisieren Sie das Datenbankschema per Hand."

#: menu
msgid "Back"
msgstr "Zur�ck"

msgid "Tree"
msgstr "Baum"

msgid "Overview"
msgstr "�bersicht"

msgid "Network"
msgstr "Netzwerk"

msgid "Show all Nets"
msgstr "Zeige alle Netze"

msgid "Add Root"
msgstr "Startpunkt hinzuf�gen"

msgid "Add root"
msgstr "Startpunkt hinzuf�gen"

msgid "Add Network"
msgstr "Netz hinzuf�gen"

msgid "Add network"
msgstr "Netz hinzuf�gen"

msgid "Import"
msgstr "Importieren"

msgid "ASN routes"
msgstr "ASN Routen"

msgid "Add network <b>%s</b>"
msgstr "Netz <b>%s</b> hinzuf�gen"

msgid "Edit network <b>%s</b>"
msgstr "Netz <b>%s</b> bearbeiten"

msgid "Separator"
msgstr "Separator"

msgid "Miscellaneous"
msgstr "Sonstiges"

msgid "Search"
msgstr "Suchen"

msgid "Exact"
msgstr "Exakt"

msgid "Show number of free subnets"
msgstr "Zeige Anzahl freier Subnetze an"

msgid "Fuzzy Search"
msgstr "Unscharfe Suche"

msgid "# of free subnets"
msgstr "# freier Subnetze"

msgid "Wildcard"
msgstr "Platzhalter"

msgid "Maintenance"
msgstr "Verwaltung"

msgid "User management"
msgstr "Benutzerverwaltung"

msgid "Group management"
msgstr "Gruppenverwaltung"

msgid "Logged in as <font id='menuBoxUsername'>%s</font>"
msgstr "Angemeldet als <font id='menuBoxUsername'>%s</font>"

msgid "Logout"
msgstr "Abmelden"

msgid "Random Tree"
msgstr "Zuf�lliger Baum"

msgid "Template management"
msgstr "Vorlagenverwaltung"

msgid "Flush cache"
msgstr "Cache leeren"

msgid "About HaCi"
msgstr "�ber HaCi"

msgid "Search free networks"
msgstr "Suche freie Netze"

#: Import
msgid "Import from DNS zonefiles"
msgstr "von DNS Zonefiles importieren"

msgid "Local Zonefile"
msgstr "Lokales Zonefile"

msgid "%i Routes found. Saved %i new Inetnums of %i found"
msgstr "%i Routen gefunden. %i neue Inetnums von %i gefundenen gesichert"

msgid "Import from Config File"
msgstr "Aus Konfig-Dateien importieren"

msgid "Source"
msgstr "Quelle"

msgid "Config"
msgstr "Konfig"

msgid "Cannot add Root"
msgstr "Kann Startpunkt nicht erstellen"

msgid "AS Number"
msgstr "AS Nummer"

msgid "Remove obsoletes"
msgstr "Alte l�schen"

msgid "Only insert new"
msgstr "Nur Neue hinzuf�gen"

msgid "No Config given!"
msgstr "Keine Konfiguration �bergeben!"

#: addRoot
msgid "Name of Root"
msgstr "Name des Startpunktes"

msgid "Description"
msgstr "Beschreibung"

msgid "Sorry, you have to give me a name!"
msgstr "Bitte gib mir einen Namen!"

msgid "Details of Root <b>%s</b>"
msgstr "Details des Startpunktes <b>%s</b>"

msgid "Number of Networks"
msgstr "Anzahl der Subnetze"

msgid "Sorry, this Root '%s' allready exists!"
msgstr "Dieser Startpunkt '%s' existiert schon!"

#: addNet
msgid "Available Variables from the Plugins"
msgstr "Verf�gbare Variablen der Plugins"

msgid "Available Variables"
msgstr "Verf�gbare Variablen"

msgid "Description Helper"
msgstr "Hilfe f�r die Beschreibung"

msgid "Insert"
msgstr "Einf�gen"

msgid "Order"
msgstr "Reihenfolge"

msgid "New Line"
msgstr "Neue Zeile"

msgid "Netaddress"
msgstr "Netzwerk Adresse"

msgid "Netmask"
msgstr "Netz Maske"

msgid "CIDR"
msgstr "CIDR"

msgid "Target Root"
msgstr "Zielpunkt"

msgid "Sorry, you have to give me a %s!"
msgstr "Ich brauche noch ein %s"

msgid "Sorry, this is not a correct Network: %s!"
msgstr "Das ist kein korrektes Netz: %s!"

msgid "Sorry, this doesn't look like a %s: '%s'!"
msgstr "Das sieht nicht wie ein %s aus: '%s'"

msgid "Access Rights"
msgstr "Zugriffsrechte"

msgid "This Network '%s' allready exists!"
msgstr "Dieses Netz '%s' existiert schon!"

msgid "Not enough rights to edit this Network '%s'"
msgstr "Nicht gen�gend Rechte vorhanden, um dieses Netz '%s' zu editieren."

msgid "Not enough rights to add this Network '%s' under '%s'"
msgstr "Nicht gen�gend Rechte vorhanden, um dieses Netz '%s' unter '%s' zu erstellen"

msgid "Def. Subnet CIDR"
msgstr "Std. Subnetz CIDR"

msgid "Default Subnet CIDR"
msgstr "Standard Subnetz CIDR"

msgid "The minimum %s size is /%i!"
msgstr "Die kleinste %s Gr��e ist /%s"

msgid "%ss can only be made from allocations with a status of %s"
msgstr "%ss k�nnen nur unter Allokationen mit einem Status von %s angelegt werden"

msgid "%ss can only be made when there's no less specific inetnum (%s) with an 'Assigned' status"
msgstr "%ss k�nnen nur angelegt werden, wenn kein weniger spezifisches inetnum (%s) mit einem 'Assigned' status vorhanden ist"

msgid "%ss can only be made when there's no more specific inetnum (%s) with an 'Assigned' status"
msgstr "%ss k�nnen nur angelegt werden, wenn kein spezifischeres inetnum (%s) mit einem 'Assigned' status vorhanden ist"

msgid "%s is a new Value for inet6num Objects and so it's not available for inetnum Objects"
msgstr "%s ist ein neuer Wert f�r inet6num Objekte. Somit ist er nicht f�r inetnum Objekte verf�gbar"

msgid "%ss can only be made when there's no less specific inetnum (%s) with a status of '%s'"
msgstr "%ss k�nnen nur angelegt werden, wenn kein weniger spezifisches inetnum (%s) mit einem Status von %s  vorhanden ist"

msgid "force"
msgstr "forcieren"

#: treeMenu
msgid "Menu"
msgstr "Men�"

msgid "Edit Menu"
msgstr "Bearbeiten"

msgid "Close Tree"
msgstr "Baum schliessen"

msgid "Jump To"
msgstr "Springe zu"

msgid "Edit"
msgstr "Bearbeiten"

msgid "Finish Edit"
msgstr "Bearbeiten beenden"

msgid "Copy"
msgstr "Kopieren"

msgid "Move"
msgstr "Verschieben"

msgid "to"
msgstr "nach"

msgid "Delete after Copy"
msgstr "L�schen nachm Kopieren"

msgid "Expand Network '%s'"
msgstr "�ffne Netzwerk '%s'"

msgid "Reduce Network '%s'"
msgstr "Schlie�e Netzwerk '%s'"

msgid "Expand Root '%s'"
msgstr "�ffne StartpunktNetzwerk '%s'"

msgid "Reduce Root '%s'"
msgstr "Schlie�e Startpunkt '%s'"

#: showNet
msgid "Show"
msgstr "Zeigen"

msgid "Show Subnets"
msgstr "Subnetze anzeigen"

msgid "# of free Subnets with CIDR '%s'"
msgstr "# freier Subnetze mit CIDR '%s'"

msgid "Routing Prefix"
msgstr "Routing Prefix"

msgid "Subnet"
msgstr "Subnetz"

msgid "Customer Address Space"
msgstr "Kundenadressbereich"

msgid "compressed"
msgstr "komprimiert"

msgid "Details of Network <b>%s</b>"
msgstr "Details des Netzes <b>%s</b>"

msgid "Details of IP <b>%s</b>"
msgstr "Details der IP <b>%s</b>"

msgid "Functions"
msgstr "Funktionen"

msgid "Reverse DNS lookup"
msgstr "R�ckw�rtsaufl�sung"

msgid "Type"
msgstr "Typ"

msgid "Type info of Network <b>%s</b>"
msgstr "Typ Informationen des Netzes <b>%s</b>"

msgid "Type info of IP <b>%s</b>"
msgstr "Typ Informationen der IP <b>%s</b>"

msgid "Split"
msgstr "Aufteilen"

msgid "Split Details"
msgstr "Aufteilungs Details"

msgid "Split Network into these pieces:"
msgstr "Netz in folgende St�cke aufteilen:"

msgid "Template for the Descriptions"
msgstr "Vorlage f�r die Beschreibungen"

msgid "Delete this network"
msgstr "Dieses Netz l�schen"

msgid "Settings for the new Networks"
msgstr "Einstellungen f�r die neuen Netze"

msgid "# of available Adresses"
msgstr "# verf�gbarer Adressen"

#: delNet
msgid "Do you really want to delete the Network '<b>%s</b>'?"
msgstr "Wollen sie wirklich das Netz '<b>%s</b>' l�schen?"

msgid "With all Subnets"
msgstr "Mit allen Subnetzen"

msgid "This network contains <b>%i</b> subnets"
msgstr "Dieses Netz beinhaltet <b>%i</b> Subnetze"

#: editRoot
msgid "Edit Root <b>%s</b>"
msgstr "Startpunkt <b>%s</b> bearbeiten"

msgid "Root"
msgstr "Startpunkt"

#: delRoot
msgid "Error while deleting '%s': %s"
msgstr "Fehler beim L�schen '%s': %s"

msgid "Successfully deleted '%s' (%i Networks deleted)"
msgstr "Erfolgreich '%s' gel�scht (%i Netze gel�scht)"

msgid "This Root has <b>%i</b> Subnets"
msgstr "Dieser Startpunkt beinhaltet <b>%i</b> Subnetze"

msgid "Do you really want to delete the Root <b>%s</b>?"
msgstr "Wollen Sie wirklich den Startpunkt <b>%s</b> l�schen?"

#: showTemplates
msgid "Templates"
msgstr "Vorlagen"

msgid "Nettype"
msgstr "Netz Typ"

msgid "There are still Entries for this Template left. Please delete them first! (%s)"
msgstr "Es sind noch Eintr�ge f�r diese Vorlage vorhanden. Bitte vorher l�schen! (%s)"

#: editTemplate
msgid "Add"
msgstr "Hinzuf�gen"

msgid "Replace"
msgstr "Ersetzen"

msgid "Template '<b>%s</b>' for '<b>%s</b>'"
msgstr "Vorlage '<b>%s</b>' f�r '<b>%s</b>'"

msgid "New "
msgstr "Neue "

msgid "Preview"
msgstr "Vorschau"

msgid "Position"
msgstr "Position"

msgid "No Template for this ID '%i' available!"
msgstr "Keine Vorlage f�r diese ID '%i' gefunden"

msgid "Parameters"
msgstr "Parameter"

msgid "Size"
msgstr "Gr��e"

msgid "Entries (separated with semicolons)"
msgstr "Eintr�ge (getrennt mit Semikolons)"

msgid "HLine"
msgstr "HLinie"

msgid "Textfield"
msgstr "Textfeld"

msgid "Textarea"
msgstr "Textbereich"

msgid "Popup-Menu"
msgstr "Popup-Men�"

msgid "Text"
msgstr "Text"

msgid "Sorry, you have to give me a Name!"
msgstr "Es muss ein Name angegeben werden!"

msgid "Do you really want to delete the Template <b>%s</b>?"
msgstr "Wollen Sie wirklich die Vorlage <b>%s</b> l�schen?"

msgid "Successfully deleted Template '%s'"
msgstr "Erfolgreich Vorlage '%s' gel�scht"

msgid "No Templates deleted. Nothing found!"
msgstr "Keine Vorlagen gel�scht. Nix gefunden!"

msgid "Rows"
msgstr "Reihen"

msgid "Columns"
msgstr "Spalten"

msgid "Created from"
msgstr "Erstellt von"

msgid "Created on"
msgstr "Erstellt am"

msgid "Modified by"
msgstr "Ge�ndert von"

msgid "Modified on"
msgstr "Ge�ndert am"

msgid "Structure"
msgstr "Struktur"

msgid "Template name"
msgstr "Vorlagen Name"

#: ShowGroups
msgid "Groups"
msgstr "Gruppen"

msgid "Group '<b>%s</b>'"
msgstr "Gruppe '<b>%s</b>'"

msgid "Permissions"
msgstr "Rechte"

#: delGroup
msgid "There are still Users in this Group left. Please remove them from this Group first! (%s)"
msgstr "Es sind noch Benutzer in der Gruppe eingetragen. Bitte tragen sie diese erstmal aus der Gruppe aus! (%s)"

msgid "No Group deleted. Nothing found!"
msgstr "Keine Gruppe gel�scht. Nix gefunden!"

msgid "You cannot delete the administrator group!"
msgstr "Die Administratorgruppe kann nicht gel�scht werden!"

msgid "Successfully deleted Group '%s'"
msgstr "Erfolgreich Gruppe '%s' gel�scht"

msgid "Do you really want to delete the Group <b>%s</b>?"
msgstr "Wollen Sie wirklich die Gruppe <b>%s</b> l�schen?"

#: editUser
msgid "User '<b>%s</b>'"
msgstr "Benutzer '<b>%s</b>'"

msgid "Group Association"
msgstr "Gruppenzugeh�rigkeit"

msgid "Password Validation"
msgstr "Passwort Wiederholung"

msgid "No Password given!"
msgstr "Kein Passwort angegeben!"

msgid "Password are not equal"
msgstr "Passw�rter sind nicht gleich!"

msgid "Password is only for buildin 'HaCi' authentication!"
msgstr "Das Passwort ist nur f�r die eingebaute 'HaCi' Authentifizierung!"

msgid "Not Authenticated!!!"
msgstr "Nicht Angemeldet!!!"

#: delUser
msgid "No User deleted. Nothing found!"
msgstr "Kein Benutzer gel�scht. Nix gefunden"

msgid "Successfully deleted User '%s'"
msgstr "User '%s' erfolgreich gel�scht"

msgid "Do you really want to delete the User <b>%s</b>?"
msgstr "Wollen sie wirklich den Benutzer '<b>%s</b>' l�schen?"

#: showAuthMeths
msgid "Authentication Management"
msgstr "Authentifizierungsverwaltung"

msgid "Authentication Methods"
msgstr "Authentifizierungs Methoden"

#: combineNets
msgid "Combine Networks"
msgstr "Netze Zusammenfassen"

msgid "%i. Possibility"
msgstr "%i. M�glichkeit"

msgid "Combine these networks"
msgstr "Diese Netze zusammenfassen"

msgid "Settings for the new Network"
msgstr "Einstellungen f�r das neue Netz"

msgid "Netaddress doesn't match!"
msgstr "Netzadresse passt nicht!"

msgid "Foreign Networks will be <br>included (e.g. '%s')"
msgstr "Fremde Netze werden mit <br>eingeschlossen (z.B. '%s')"

msgid "This Network allready exists!"
msgstr "Dieses Netz existiert schon!"

#: pluginMgmt
msgid "Plugin management"
msgstr "Pluginverwaltung"

msgid "Configure Plugin '%s'"
msgstr "Konfiguriere Plugin '%s'"

#: plugins
msgid "Global Configuration of '%s'"
msgstr "Globale Konfiguration von '%s'"

msgid "Configuration of '%s' for '%s'"
msgstr "Konfiguration von '%s' f�r '%s'"

msgid "Status of Host"
msgstr "Status des Hosts"

msgid "Alive"
msgstr "Lebendig"

msgid "alive"
msgstr "lebendig"

msgid "Dead"
msgstr "Tot"

msgid "Ping Info for %s"
msgstr "Ping Info f�r %s"

msgid "active"
msgstr "aktiv"

msgid "default"
msgstr "standard"

msgid "Default"
msgstr "Standard"

msgid "on Demand"
msgstr "auf Abruf"

msgid "recurrent"
msgstr "regelm��ig"

msgid "last Run"
msgstr "letzter Aufruf"

msgid "Configure"
msgstr "Konfigurieren"

msgid "Reset last run"
msgstr "Letzen Aufruf resetten"

#plugins
msgid "DNS PTR-Record for current IP address"
msgstr "DNS PTR-Record f�r die jeweilige IP Adresse"

msgid "Ping Status of current IP address (dead|alive)"
msgstr "Ping Status der jeweiligen IP Adresse (dead|alive)"

msgid "Mac Address related to the IP address"
msgstr "Mac Adresse der IP Adresse"

msgid "Infos for Plugin '%s'"
msgstr "Informationen f�r Plugin '%s'"

msgid "This Plugin will ping associated IP adresses and saves its status in DB. After that, it will collect the corresponding MAC addresses with SNMP from a specific router and save the result in DB, too. In the Output there were all IP addresses listed with their status and MAC address if available. With this method you catch all available Hosts, even if they deny ping."
msgstr "Dieses Plugin pingt alle ihm zugeordneten IP Adressen und speichert deren Status in der DB. Danach sammelt es die passenden MAC Adressen mittels SNMP von einem bestimmten Router und speichert die Ergebnisse ebenfalls in der DB. In der Ausgabe werden alle IP Adressen mit ihrem Status und, wenn vorhanden, ihrer MAC Adresse dargestellt. Mit dieser Methode k�nnen alle verf�gbaren Ger�te erkannt werden, auch wenn sie eigentlich Ping verbieten."

msgid "This Plugin runs recurrent in background and collects DNS PTR-Records for its associated IP Addresses an saves them in DB. In the Output will all IP Adresses be listet with their corresponding PTR-Records."
msgstr "Dieses Plugin l�uft regelm��ig im Hintergrund und sammelt DNS PTR-Records f�r alle ihm zugeordneten IP Adressen und speichert diese in der DB. In der Ausgabe werden alle IP Adressen mit deren zugeh�rigen PTR-Records aufgelistet."

msgid "This Plugin queries on demand the DNS PTR-Record for the current IP adress and displays it."
msgstr "Dieses Plugin fragt auf Abruf den DNS PTR-Record f�r die jeweilige IP Adresse ab und zeigt ihn an."

msgid "This Plugins pings the current IP adress on Demand and displays its status."
msgstr "Dieses Plugin pingt die jeweilige IP Adresse auf Abruf und zeigt deren Status an."

msgid "This Plugin runns recurrent in background and pings all associated IP addresses. In the Output it will display all IP addresses with their Status."
msgstr "Dieses Plugin l�uft regelm��ig im Hintergrund und pingt alle ihm zugeordneten IP Adressen. In der Ausgabe werden alle IP Adressen mit ihrem Status angezeigt."

msgid "This Plugin queries on Demand the Whois DB for the current network and displays the Result."
msgstr "Dieses Plugin fragt auf Abruf die Whois DB f�r das jeweilige Netzwerk ab und zeigt das Ergebnis an."

msgid "IP addresses"
msgstr "IP Adressen"

msgid "MACs found"
msgstr "MACs gefunden"

msgid "ARP Entry"
msgstr "ARP Eintrag"

msgid "Nr of Bytes (0-1024)"
msgstr "Byteanzahl (0-1024)"

msgid "Timeout"
msgstr "Zeit�berschreitung"

msgid "Protocoll"
msgstr "Protokoll"

msgid "Router for SNMP-Connection"
msgstr "Router f�r die SNMP Verbindung"

msgid "With Subnetworks"
msgstr "Inklusive Subnetze"

msgid "Only IP addresses"
msgstr "Nur IP Adressen"

msgid "Max Depth"
msgstr "Maximale Tiefe"

msgid "Runtime"
msgstr "Laufzeit"

msgid "sec"
msgstr "sek"

msgid "Close Error Details"
msgstr "Schlie�e Fehler Details"

msgid "Error Details for Plugin %s"
msgstr "Fehler Details f�r Plugin %s"

msgid "Error Details"
msgstr "Fehler Details"

msgid "Error"
msgstr "Fehler"

msgid "IP address"
msgstr "IP Adresse"

msgid "Error while loading Plugin '%s'. Details in Error Logfile."
msgstr "Fehler beim Laden des Plugins '%s'. Details stehen im Fehler Log."

msgid "This doesn't look like a network '%s'."
msgstr "Das sieht nicht wie ein Netz aus '%s'."

msgid "Program Whois '%s' isn't executable"
msgstr "Das Whois Programm '%s' ist nicht ausf�hrbar"

msgid "This doesn't look like an IP address '%s'."
msgstr "Das sieht nicht wie eine IP Adresse aus '%s'."

msgid "Defaults for mode 'recurrent'"
msgstr "Standards f�r den Modus 'regelm��ig'"

msgid "Defaults for mode 'on Demand'"
msgstr "Standards f�r den Modus 'auf Abruf'"

msgid "The HaCi Daemon is not started."
msgstr "Der HaCi Daemon ist nicht gestartet."

#: showSubnets
msgid "Show free Subnets of '%s' with CIDR '%s'"
msgstr "Zeige freie Subnetze von '%s' mit CIDR '%s'"

msgid "CIDR (Subnet Size)"
msgstr "CIDR (Subnetz Gr��e)"

msgid "Free Subnets"
msgstr "Freie Subnetze"

msgid "No free Subnets with this CIDR available"
msgstr "Keine freien Subnetze mit dieser CIDR verf�gbar"

msgid "Create"
msgstr "Erstellen"

msgid "No."
msgstr "Nr."

#: showSettings
msgid "Settings"
msgstr "Einstellungen"

msgid "Change own Password"
msgstr "Eigenes Passwort �ndern"

msgid "Old Password"
msgstr "Altes Passwort"

msgid "New Password"
msgstr "Neues Passwort"

msgid "Old Password is not correct!"
msgstr "Altes Passwort ist nicht richtig!"

msgid "Sorry, Username not found!"
msgstr "Benutzername nicht gefunden!"

msgid "Successfully changed Password"
msgstr "Passwort erfolgreich ge�ndert"

msgid "Cannot change Password: %s"
msgstr "Konnte Passwort nicht �ndern : %s"

msgid "View"
msgstr "Ansicht"

msgid "View Settings"
msgstr "Ansichtseinstellungen"

msgid "Show Tree Structure"
msgstr "Baumstruktur anzeigen"

msgid "Errors while updating Setting for '%s': %s"
msgstr "W�rend die Einstellungen f�r '%s' aktualisiert wurden, sind Fehler aufgetreten: %s"

#: audit
msgid "Show audit logs"
msgstr "Zeige Auditaufzeichnungen"

msgid "Audit logs"
msgstr "Auditaufzeichnungen"

msgid "Sort by"
msgstr "Sortieren nach"

msgid "Reverse"
msgstr "Umgekehrt"

msgid "Timestamp"
msgstr "Datum"

msgid "Action"
msgstr "Aktion"

msgid "Object"
msgstr "Objekt"

msgid "Value"
msgstr "Wert"

msgid "Number of entries"
msgstr "Anzahl der Eintr�ge"

msgid "Page (%d .. %d)"
msgstr "Seite (%d .. %d)"

msgid "Overwrite existing"
msgstr "�berschreibe existierende"

msgid "Add free network"
msgstr "Freies Netz hinzuf�gen"

msgid "Search for free networks"
msgstr "Suche nach freien Netzen"

msgid "Amount"
msgstr "Anzahl"

msgid "Result"
msgstr "Ergebnis"

msgid "No free networks found"
msgstr "Keine freien Netze gefunden"

msgid "Never"
msgstr "Nie"

msgid "No changes"
msgstr "Keine �nderungen"

msgid "Sorry, the network '%s' is still locked for '%i' seconds till '%s'!"
msgstr "Das Netzwerk '%s' ist noch f�r '%i' Sekunden gesperrt bis '%s'!"

msgid "Error while deleting network lock '%i': %s"
msgstr "Fehler beim Entfernen der Netzwerk-Sperre '%i': %s"

msgid "Lock network for X seconds"
msgstr "Sperre Netzwerk f�r X Sekunden"

msgid "Network lock time must be provided in seconds not '%s'"
msgstr "Netzwerksperre muss in Sekunden angegeben werden, nicht '%s'"

msgid "All"
msgstr "Alle"

msgid "Inherit from supernet"
msgstr "Vom Supernetz erben"

msgid "Export Subnets"
msgstr "Subnetze exportieren"

msgid "# of assigned subnets"
msgstr "# vergebener Subnetze"

msgid "%s: Status (%s) not known"
msgstr "%s: Status (%s) nicht bekannt"

msgid "seperate<br>by space"
msgstr "trennen mit<br>Leerzeichen"

msgid "AND"
msgstr "UND"

msgid "OR"
msgstr "ODER"

msgid "marked: NOT inherited"
msgstr "markiert: NICHT vererbt"

msgid "Enable internal account"
msgstr "internes Konto aktivieren"

msgid "Internal account is disabled."
msgstr "Internes Konto is deaktiviert"
