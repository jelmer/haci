domain "HaCi"

#: login
msgid "Authentication"
msgstr "Autenticazione"

msgid "Language"
msgstr "Lingua"

msgid "Username"
msgstr "Utente"

msgid "Password"
msgstr "Password"

msgid "Authentication failed!"
msgstr "Autenticazione fallita!"

msgid "Authentication failed: User not found!"
msgstr "Autenticazione fallita: non trovo l'Utente!"

msgid "No User given"
msgstr "Non hai specificato l'Utente"

msgid "Session is expired"
msgstr "Sessione scaduta"

msgid "Authentication failed: Database Error!"
msgstr "Autenticazione fallita: Errore del Database!"

msgid "Authentication failed: Password doesn't match!"
msgstr "Autenticazione fallita: Password errata!"

msgid "Support"
msgstr "Supporto"

#: misc
msgid "or"
msgstr "o"

msgid "NEW"
msgstr "NUOVO"

msgid "Users"
msgstr "Utenti"

msgid "New User"
msgstr "Nuovo Utente"

msgid "New Group"
msgstr "Nuovo Gruppo"

msgid "New Template"
msgstr "Nuovo Template"

msgid "Login"
msgstr "Accesso"

msgid "Save result under"
msgstr "Salva i risultati come"

msgid "Right"
msgstr "Destra"

msgid "Left"
msgstr "Sinistra"

msgid "Compare"
msgstr "Confronta"

msgid "Target"
msgstr "Destinazione"

msgid "Search,Compare"
msgstr "Cerca, Confronta"

msgid "Import ASN Routes"
msgstr "Importa ASN routes"

msgid "Import DNS Zonefile"
msgstr "Importa DNS Zonefile"

msgid "No IP Addresses found for Origin '%s'."
msgstr "Non trovo indirizzi IP per Origin '%s'."

msgid "Zone transfer failed: %s"
msgstr "Zone transfer fallito: %s"

msgid "Import Config"
msgstr "Importa Configurazioni"

msgid "No File given!"
msgstr "Non hai specificato il File!"

msgid "Cannot open File in Binmode: %s"
msgstr "Non posso aprire il File in Binmode: %s"

msgid "%i IP Addresses found for Origin '%s'. %i were successfully saved under Root '%s'"
msgstr "Trovati %i indirizzi IP per Origin '%s'. %i salvati con success sotto la Radice '%s'."

msgid "No Origin given or found!"
msgstr "Non riesco a determinare Origin!"

msgid "Edit Network"
msgstr "Modifica la Rete"

msgid "Show Root Details"
msgstr "Mostra i dettagli della Radice"

msgid "Show Roots"
msgstr "Mostra le Radici"

msgid "Show Networks"
msgstr "Mostra le Reti"

msgid "Show Network Details"
msgstr "Mostra i dettagli della Rete"

msgid "Edit Root"
msgstr "Modifica la Radice"

msgid "Root Name"
msgstr "Nome della Radice"

msgid "Cannot read ConfigFile '%s': %s"
msgstr "Non riesco a leggere il file di configurazione '%s': %s"

msgid "Abort"
msgstr "Interrompi"

msgid "Submit"
msgstr "Invia"

msgid "Change"
msgstr "Cambia"

msgid "New"
msgstr "Nuovo"

msgid "Delete"
msgstr "Cancella"

msgid "Combine"
msgstr "Combina"

msgid "DNS Info for %s"
msgstr "Informazioni DNS per %s"

msgid "RIPE Info for %s"
msgstr "Informazioni da RIPE per %s"

msgid "unknown"
msgstr "sconosciuto"

msgid "Nothing found"
msgstr "Non trovo nulla"

msgid "Error while deleting '%s' from '%s': %s"
msgstr "Errore cercando di cancellare '%s' da '%s': %s"

msgid "Successfully deleted '%s' from '%s' (%i Networks deleted)"
msgstr "Cancellazione di '%s' da '%s' riuscita (%i Reti cancellate)"

msgid "None"
msgstr "Nessuno"

msgid "invisible"
msgstr "invisibile"

msgid "read"
msgstr "leggi"

msgid "write"
msgstr "scrivi"

msgid "Group"
msgstr "Gruppo"

msgid "CSV Preview"
msgstr "Anteprima CSV"

msgid "Cannot parse any CSV Data"
msgstr "Non riesco a legger alcun dato CSV"

msgid "[ permission denied ]"
msgstr "[ permesso negato ]"

msgid "This Network is locked"
msgstr "Questa Rete e` bloccata"

msgid "This Network is reserved"
msgstr "Questa Rete e` riservata"

msgid "This Network is free"
msgstr "Questa Rete e` libera"

msgid "Contact"
msgstr "Contatto"

#: menu
msgid "Back"
msgstr "Indietro"

msgid "Tree"
msgstr "Albero"

msgid "Overview"
msgstr "Panoramica"

msgid "Network"
msgstr "Rete"

msgid "Show all Nets"
msgstr "Mostra tutte le Reti"

msgid "Add root"
msgstr "Aggiungi una Radice"

msgid "Add network"
msgstr "Aggiungi una Rete"

msgid "Import"
msgstr "Importa"

msgid "ASN routes"
msgstr "ASN Routes"

msgid "Add network <b>%s</b>"
msgstr "Aggiungu la Rete <b>%s</b>"

msgid "Edit network <b>%s</b>"
msgstr "Modifica la Rete <b>%s</b>"

msgid "Separator"
msgstr "Separatore"

msgid "Miscellaneous"
msgstr "Miscellanea"

msgid "Search"
msgstr "Cerca"

msgid "Exact"
msgstr "Esatto"

msgid "Fuzzy Search"
msgstr "Ricerca Fuzzy"

msgid "Wildcard"
msgstr "Wildcard"

msgid "Maintenance"
msgstr "Manutenzione"

msgid "User management"
msgstr "Gestione Utenti"

msgid "Group management"
msgstr "Gestione Gruppi"

msgid "Logged in as <font id='menuBoxUsername'>%s</font>"
msgstr "Entrato come <font id='menuBoxUsername'>%s</font>"

msgid "Logout"
msgstr "Esci"

msgid "Random Tree"
msgstr "Albero Casuale"

msgid "Template management"
msgstr "Gestione Template"

msgid "Flush cache"
msgstr "Svuota la Cache"

msgid "About HaCi"
msgstr "About HaCi"

#: Import
msgid "Import from DNS Zonefiles"
msgstr "Importa da DNS Zonefiles"

msgid "Local Zonefile"
msgstr "Zonefile locale"

msgid "%i Routes found. Saved %i new Inetnums of %i found"
msgstr "Trovate %i Rotte. Salvati %i nuovi Inetnums di %i"

msgid "Import from Config File"
msgstr "Importa da File di Configurazione"

msgid "Source"
msgstr "Sorgente"

msgid "Config"
msgstr "Configurazione"

msgid "Cannot add Root"
msgstr "Non posso aggiungere la Radice"

msgid "AS Number"
msgstr "AS Number"

msgid "Remove obsoletes"
msgstr "Rimuovi obsoleti"

msgid "Only insert new"
msgstr "Inserisci solo i nuovi"

msgid "No Config given!"
msgstr "Non hai specificato il File di Configurazione!"

#: addRoot
msgid "Name of Root"
msgstr "Nome della Radice"

msgid "Description"
msgstr "Descrizione"

msgid "Sorry, you have to give me a name!"
msgstr "Per favore specifica un nome!"

msgid "Details of Root <b>%s</b>"
msgstr "Dettagli della Radice <b>%s</b>"

msgid "Number of Networks"
msgstr "Numero di Reti"

msgid "Sorry, this Root '%s' allready exists!"
msgstr "Spiacente, esiste gia' una root '%s'!"

#: addNet
msgid "Available Variables from the Plugins"
msgstr "Variabili disponibili dai Plugin"

msgid "Description Helper"
msgstr "Aiuto per la Descrizione"

msgid "Insert"
msgstr "Inserisci"

msgid "Order"
msgstr "Ordina"

msgid "New Line"
msgstr "Nuova Linea"

msgid "Netaddress"
msgstr "Indirizzo di Rete"

msgid "Netmask"
msgstr "Netmask"

msgid "CIDR"
msgstr "CIDR"

msgid "Target Root"
msgstr "Radice di destinazione"

msgid "Sorry, you have to give me a %s!"
msgstr "Spiacente, mi devi dare un %s"

msgid "Sorry, this is not a correct Network: %s!"
msgstr "Spiacente, questa Rete non e` corretta: %s!"

msgid "Sorry, this doesn't look like a %s: '%s'!"
msgstr "Spiacente, questo non sembra essere un %s: '%s'"

msgid "Access Rights"
msgstr "Diritti d'accesso"

msgid "This Network '%s' allready exists!"
msgstr "Questa Rete '%s' esiste gia'!"

msgid "Not enough rights to edit this Network '%s'"
msgstr "Non hai abbastanza privilegi per modificare questa Rete '%s'."

msgid "Not enough rights to add this Network '%s' under '%s'"
msgstr "Non hai abbastanza privilegi per aggiungere la Rete '%s' sotto '%s'"

msgid "Def. Subnet CIDR"
msgstr "Def. Subnet CIDR"

msgid "Default Subnet CIDR"
msgstr "Default Subnet CIDR"

msgid "The minimum %s size is /%i!"
msgstr "La dimensione minima di %s e` /%s"

msgid "%ss can only be made from allocations with a status of %s"
msgstr "%ss puo' essere definito solo a partire da allocazioni con stato %s"

msgid "%ss can only be made when there's no less specific inetnum (%s) with an 'Assigned' status"
msgstr "%ss puo' essere definito solo se non ci sono inetnum meno specifici (%s) con stato 'Assigned'"

msgid "%ss can only be made when there's no more specific inetnum (%s) with an 'Assigned' status"
msgstr "%ss puo' essere definito solo se non ci sono inetnum piu' specifici (%s) con stato 'Assigned"

msgid "%s is a new Value for inet6num Objects and so it's not available for inetnum Objects"
msgstr "%s e` un nuovo valore per oggetti inet6num e non e` disponibile per oggetti inetnum"

msgid "%ss can only be made when there's no less specific inetnum (%s) with a status of '%s'"
msgstr "%ss puo' essere definito solo se non ci sono inetnum meno specifici (%s) con stato '%s'"

msgid "force"
msgstr "forza"

#: treeMenu
msgid "Menu"
msgstr "Menu'"

msgid "Edit Menu"
msgstr "Menu' delle modifiche"

msgid "Close Tree"
msgstr "Chiudi l'albero"

msgid "Jump To"
msgstr "Salta a"

msgid "Edit"
msgstr "Modifica"

msgid "Finish Edit"
msgstr "Termina le modifiche"

msgid "Copy"
msgstr "Copia"

msgid "Move"
msgstr "Sposta"

msgid "to"
msgstr "a"

msgid "Delete after Copy"
msgstr "Cancella dopo la Copia"

msgid "Expand Network '%s'"
msgstr "Espandi la Rete '%s'"

msgid "Reduce Network '%s'"
msgstr "Riduci la Rete '%s'"

msgid "Expand Root '%s'"
msgstr "Espandi la Radice '%s'"

msgid "Reduce Root '%s'"
msgstr "Riduci la Radice '%s'"

#: showNet
msgid "Show"
msgstr "Mostra"

msgid "Show Subnets"
msgstr "Mostra le Sottoreti"

msgid "# of free Subnets with CIDR '%s'"
msgstr "# di sottoreti libere con CIDR '%s'"

msgid "Routing Prefix"
msgstr "Routing Prefix"

msgid "Subnet"
msgstr "Sottorete"

msgid "Customer Address Space"
msgstr "Spazio di indirizzamento per Clienti"

msgid "compressed"
msgstr "compresso"

msgid "Details of Network <b>%s</b>"
msgstr "Dettagli della Rete <b>%s</b>"

msgid "Functions"
msgstr "Funzioni"

msgid "Reverse DNS lookup"
msgstr "Interroga il DNS inverso"

msgid "Type"
msgstr "Tipo"

msgid "Type Infos of Network <b>%s</b>"
msgstr "Informazioni sul tipo di Rete <b>%s</b>"

msgid "Split"
msgstr "Suddividi"

msgid "Split Details"
msgstr "Dettagli della suddivisione"

msgid "Split Network into these pieces:"
msgstr "Suddividi la Rete in questi pezzi:"

msgid "Template for the Descriptions"
msgstr "Maschera per le Descrizioni"

msgid "Delete this network"
msgstr "Cancella questa Rete"

msgid "Settings for the new Networks"
msgstr "Impostazioni per le nuove Reti"

msgid "# of available Adresses"
msgstr "# di indirizzi disponibili"

#: delNet
msgid "Do you really want to delete the Network '<b>%s</b>'?"
msgstr "Vuoi veramente cancellare la Rete '<b>%s</b>'?"

msgid "With all Subnets"
msgstr "Con tutte le Sottoreti"

msgid "This network contains <b>%i</b> subnets"
msgstr "Questa Rete contiene <b>%i</b> sottoreti"

#: editRoot
msgid "Edit Root <b>%s</b>"
msgstr "Modifica la Radice <b>%s</b>"

msgid "Root"
msgstr "Radice"

#: delRoot
msgid "Error while deleting '%s': %s"
msgstr "Errore durante la cancellazione di '%s': %s"

msgid "Successfully deleted '%s' (%i Networks deleted)"
msgstr "'%s' cancellata con successo (%i reti cancellate)"

msgid "This Root has <b>%i</b> Subnets"
msgstr "Questa Radice contiene <b>%i</b> Sottoreti"

msgid "Do you really want to delete the Root <b>%s</b>?"
msgstr "Vuoi cancellare veramente la Radice <b>%s</b>"

#: showTemplates
msgid "Templates"
msgstr "Maschere"

msgid "Nettype"
msgstr "Tipo di Rete"

msgid "There are still Entries for this Template left. Please delete them first! (%s)"
msgstr "Ci sono ancora dati per questa Maschera. Prima cancellali! (%s)"

#: editTemplate
msgid "Add"
msgstr "Aggiungi"

msgid "Replace"
msgstr "Rimpiazza"

msgid "Template '<b>%s</b>' for '<b>%s</b>'"
msgstr "Maschera '<b>%s</b>' per '<b>%s</b>'"

msgid "New "
msgstr "Nuovo "

msgid "Preview"
msgstr "Anteprima"

msgid "Position"
msgstr "Posizione"

msgid "No Template for this ID '%i' available!"
msgstr "Non ci sono Maschere on ID '%i'!"

msgid "Parameters"
msgstr "Parametri"

msgid "Size"
msgstr "Dimensione"

msgid "Entries (separated with semicolons)"
msgstr "Dati (separati da punto e virgola)"

msgid "HLine"
msgstr "HLine"

msgid "Textfield"
msgstr "Campo di testo"

msgid "Textarea"
msgstr "Area di testo"

msgid "Popup-Menu"
msgstr "Popup-Menu'"

msgid "Text"
msgstr "Testo"

msgid "Sorry, you have to give me a Name!"
msgstr "Devi fornirmi un nome!"

msgid "Do you really want to delete the Template <b>%s</b>?"
msgstr "Vuoi veramente cancellare la Maschera <b>%s</b>?"

msgid "Successfully deleted Template '%s'"
msgstr "Maschera '%s' cancellata con successo"

msgid "No Templates deleted. Nothing found!"
msgstr "Nessuna Maschera cancellata, non ne ho trovate!"

msgid "Rows"
msgstr "Righe"

msgid "Columns"
msgstr "Colonne"

msgid "Created from"
msgstr "Creatp da"

msgid "Created on"
msgstr "Creato il"

msgid "Modified by"
msgstr "Modificato da"

msgid "Modified on"
msgstr "Modificato il"

msgid "Structure"
msgstr "Struttura"

#: ShowGroups
msgid "Groups"
msgstr "Gruppi"

msgid "Group '<b>%s</b>'"
msgstr "Gruppo '<b>%s</b>'"

msgid "Permissions"
msgstr "Permessi"

#: delGroup
msgid "There are still Users in this Group left. Please remove them from this Group first! (%s)"
msgstr "Ci sono ancora Utenti in questo Gruppo. Li devi prima rimuovere dal Gruppo! (%s)"

msgid "No Group deleted. Nothing found!"
msgstr "Nessun gruppo cancellato, non ne ho trovati!"

msgid "Successfully deleted Group '%s'"
msgstr "Gruppo '%s' cancellato con successo"

msgid "Do you really want to delete the Group <b>%s</b>?"
msgstr "Vuoi veramente cancellare il Gruppo <b>%s</b>?"

#: editUser
msgid "User '<b>%s</b>'"
msgstr "Utente '<b>%s</b>'"

msgid "Group Association"
msgstr "Associazione al Gruppo"

msgid "Password Validation"
msgstr "Verifica Password"

msgid "No Password given!"
msgstr "Manca la Password!"

msgid "Password are not equal"
msgstr "Le Password non sono uguali!"

msgid "Password is only for buildin 'HaCi' authentication!"
msgstr "La Password e` solo per l'autenticazione incorporata in 'HaCi'!"

msgid "Not Authenticated!!!"
msgstr "Non Autenticato!!!"

#: delUser
msgid "No User deleted. Nothing found!"
msgstr "Nessun Utente cancellato, non ne ho trovati!"

msgid "Successfully deleted User '%s'"
msgstr "Utente '%s' cancellato con successo"

msgid "Do you really want to delete the User <b>%s</b>?"
msgstr "Vuoi veramente cancellare l'Utente '<b>%s</b>'?"

#: showAuthMeths
msgid "Authentication Management"
msgstr "Gestione dell'Autenticazione"

msgid "Authentication Methods"
msgstr "Metodi di Autenticazione"

#: combineNets
msgid "Combine Networks"
msgstr "Combina le Reti"

msgid "%i. Possibility"
msgstr "%i. Possibilita'"

msgid "Combine these networks"
msgstr "Combina queste Reti"

msgid "Settings for the new Network"
msgstr "Impostazioni per la nuova Rete"

msgid "Netaddress doesn't match!"
msgstr "L'indirizzo di rete non corrisponde!"

msgid "Foreign Networks will be <br>included (e.g. '%s')"
msgstr "Reti Estranee saranno <br>incluse (es. '%s')"

msgid "This Network allready exists!"
msgstr "Questa Rete esiste gia`!"

#: pluginMgmt
msgid "Plugin management"
msgstr "Gestione Plugin"

msgid "Configure Plugin '%s'"
msgstr "Configura il Plugin '%s'"

#: plugins
msgid "Global Configuration of '%s'"
msgstr "Configurazione globale di'%s'"

msgid "Configuration of '%s' for '%s'"
msgstr "Configurazione di '%s' per '%s'"

msgid "Status of Host"
msgstr "Stato degli Host"

msgid "Alive"
msgstr "Attivo"

msgid "alive"
msgstr "attivo"

msgid "Dead"
msgstr "Inattivo"

msgid "Ping Info for %s"
msgstr "Informazioni Ping per %s"

msgid "active"
msgstr "attivo"

msgid "default"
msgstr "default"

msgid "Default"
msgstr "Default"

msgid "on Demand"
msgstr "a richiesta"

msgid "recurrent"
msgstr "ricorrente"

msgid "last Run"
msgstr "ultima esecuzione"

msgid "Configure"
msgstr "Configura"

msgid "Reset last run"
msgstr "Azzera l'orario di ultima esecuzione"

#plugins
msgid "DNS PTR-Record for current IP address"
msgstr "DNS PTR-Record per l'indirizzo IP corrente"

msgid "Ping Status of current IP address (dead|alive)"
msgstr "Stato del Ping Status per l'indirizzo IP corrente (attivo|inattivo)"

msgid "Mac Address related to the IP address"
msgstr "Mac Address relativo all'indirizzo IP"

msgid "Infos for Plugin '%s'"
msgstr "Informazioni per il Plugin '%s'"

msgid "This Plugin will ping associated IP adresses and saves its status in DB. After that, it will collect the corresponding MAC addresses with SNMP from a specific router and save the result in DB, too. In the Output there were all IP addresses listed with their status and MAC address if available. With this method you catch all available Hosts, even if they deny ping."
msgstr "Questo Plugin esegue un Ping agli indirizzi IP associati e salva lo stato nel DB. Dopodiche' raccoglie i corrispondenti MAC address con SNMP da un router specifico e salva anche questi risultati nel DB. Fornisce quindi la lista di tutti gli indirizzi IP con il loro stato e il MAC address se disponibile. Con questo metodo puoi analizzare tutti gli host, anche se non rispondono ai ping."

msgid "This Plugin runs recurrent in background and collects DNS PTR-Records for its associated IP Addresses an saves them in DB. In the Output will all IP Adresses be listet with their corresponding PTR-Records."
msgstr "Questo Plugin viene eseguito a tempo in background e raccoglie i record PTR dal DNS per gli indirizzi IP associati e li salva nel DB. Fornisce quindi la lista di tutti gli indirizzi IP con il corrispondente record PTR."

msgid "This Plugin queries on demand the DNS PTR-Record for the current IP adress and displays it."
msgstr "Questo Plugin interroga a richiestsa il DNS e mostra il record PTR dell'indirizzo IP corrente."

msgid "This Plugins pings the current IP adress on Demand and displays its status."
msgstr "Questo Plugin eseguoe un Ping a richiesta e mostra lo stato dell'indirizzo IP corrente."

msgid "This Plugin runns recurrent in background and pings all associated IP addresses. In the Output it will display all IP addresses with their Status."
msgstr "Questo Plugin viene eseguito a tempo in background ed esegue il Ping per tutti gli indirizzi IP associati. Fornisce quindi la lista degli indirizzi IP e il loro stato."

msgid "This Plugin queries on Demand the Whois DB for the current network and displays the Result."
msgstr "Questo Plugin interroga a richiesta il DB Whois per la rete corrente e mostra il risultato."

msgid "IP addresses"
msgstr "Indirizzi IP"

msgid "MACs found"
msgstr "MAC trovati"

msgid "ARP Entry"
msgstr "ARP Entry"

msgid "Nr of Bytes (0-1024)"
msgstr "N. di Byte (0-1024)"

msgid "Timeout"
msgstr "Tempo scaduto"

msgid "Protocoll"
msgstr "Protocollo"

msgid "Router for SNMP-Connection"
msgstr "Router per le funzioni SNMP"

msgid "With Subnetworks"
msgstr "Includi le Sottoreti"

msgid "Only IP addresses"
msgstr "Solo gli indirizzi IP"

msgid "Max Depth"
msgstr "Livello massimo"

msgid "Runtime"
msgstr "Tempo di esecuzione"

msgid "sec"
msgstr "s"

msgid "Close Error Details"
msgstr "Chiudi il dettaglio degli errori"

msgid "Error Details for Plugin %s"
msgstr "Dettaglio degli errori per il Plugin %s"

msgid "Error Details"
msgstr "Dettaglio degli errori"

msgid "Error"
msgstr "Errori"

msgid "IP address"
msgstr "Indirizzo IP"

msgid "Error while loading Plugin '%s'. Details in Error Logfile."
msgstr "Errore nel caricamento del Plugin '%s'. I dettagli sono nel Log degli Errori."

msgid "This doesn't look like a network '%s'."
msgstr "Questa non sembra una rete '%s'."

msgid "Program Whois '%s' isn't executable"
msgstr "Il programma Whois '%s' non e` eseguibile"

msgid "This doesn't look like an IP address '%s'."
msgstr "Questo non sembra un indirizzo IP '%s'."

msgid "Defaults for mode 'recurrent'"
msgstr "Default per la modalita' 'ricorrente'"

msgid "Defaults for mode 'on Demand'"
msgstr "Default per la modalita' 'a richiesta'"

msgid "The HaCi Daemon is not started."
msgstr "HaCi Daemon non e` stato avviato."

#: showSubnets
msgid "Show free Subnets of '%s' with CIDR '%s'"
msgstr "Mostra le Sottoreti libere di '%s' con CIDR '%s'"

msgid "CIDR (Subnet Size)"
msgstr "CIDR (dimensione della Sottorete)"

msgid "Free Subnets"
msgstr "Sottoreti Libere"

msgid "No free Subnets with this CIDR available"
msgstr "Non ci sono Sottoreti libere con questo CIDR"

msgid "Create"
msgstr "Crea"

msgid "No."
msgstr "No."

#: showSettings
msgid "Settings"
msgstr "Impostazioni"

msgid "Change own Password"
msgstr "Cambia la propria Password"

msgid "Old Password"
msgstr "Vecchia Password"

msgid "New Password"
msgstr "Nuova Password"

msgid "Old Password is not correct!"
msgstr "La vecchia Password non e` corretta!"

msgid "Sorry, Username not found!"
msgstr "Utente non trovato!"

msgid "Successfully changed Password"
msgstr "Password cambiata con successo"

msgid "Cannot change Password: %s"
msgstr "Non posso cambiare la Password : %s"

msgid "View"
msgstr "Mostra"

msgid "View Settings"
msgstr "Mostra le Impostazioni"

msgid "Show Tree Structure"
msgstr "Mostra la struttura dell'Albero"

msgid "Errors while updating Setting for '%s': %s"
msgstr "Errori durante l'aggiornamento delle impostazioni per '%s': %s"
