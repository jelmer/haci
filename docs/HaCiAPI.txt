__HaCi_SOAP_API__
  Available methods and their parameters, return values and a short description

search :
    _IN username        $string   Username
    _IN password        $string   Password
    _IN searchString    $string   Search String
    _IN state           $string   (optional) (One of: UNSPECIFIED, ALLOCATED PA, ALLOCATED PI, ALLOCATED UNSPECIFIED, SUB-ALLOCATED PA, LIR-PARTITIONED PA, LIR-PARTITIONED PI, EARLY-REGISTRATION, NOT-SET, ASSIGNED PA, ASSIGNED PI, ASSIGNED ANYCAST, ALLOCATED-BY-RIR, ALLOCATED-BY-LIR, ASSIGNED, RESERVED, LOCKED, FREE)
    _IN exact           $boolean  (optional) Search for the Exact search String?
    _IN fuzzy           $boolean  (optional) Fuzzy search?
    _IN template        $string   (optional) isolate your Search by defining a Template
    _IN templateQueries $string   (optional) Define special Queries for the specified Template. spererated by semicolon. E.g.: value1=foo;value2=bar
    _IN root            $string   (optional) Define special root to search in
    _IN tags            $string   (optional) return only networks with this tags, seperate with spaces (i.e.: 'OR foo bar', 'AND foo bar')
    _RETURN             @HaCi::SOAP::Type::network
    _DOC                This is a search function

getFreeSubnets :
    _IN username  $string  Username
    _IN password  $string  Password
    _IN root      $string  Root Name
    _IN supernet  $string  Supernet (e.g. 192.168.0.0/24)
    _IN size      $int     Subnet CIDR (e.g. 29)
    _IN amount    $int     (optional) Amount of returned Networks (e.g. 1)
    _RETURN       @HaCi::SOAP::Type::network
    _DOC          This Service returns all free Subnets of a certain size from a given Supernet

getFreeSubnetsFromSearch :
    _IN username        $string   Username
    _IN password        $string   Password
    _IN searchString    $string   Search String
    _IN state           $string   (optional) (One of: UNSPECIFIED, ALLOCATED PA, ALLOCATED PI, ALLOCATED UNSPECIFIED, SUB-ALLOCATED PA, LIR-PARTITIONED PA, LIR-PARTITIONED PI, EARLY-REGISTRATION, NOT-SET, ASSIGNED PA, ASSIGNED PI, ASSIGNED ANYCAST, ALLOCATED-BY-RIR, ALLOCATED-BY-LIR, ASSIGNED, RESERVED, LOCKED, FREE)
    _IN exact           $boolean  (optional) Search for the Exact search String?
    _IN fuzzy           $boolean  (optional) Fuzzy search?
    _IN template        $string   (optional) isolate your Search by defining a Template
    _IN templateQueries $string   (optional) Define special Queries for the specified Template. spererated by semicolon. E.g.: value1=foo;value2=bar
    _IN size            $int      Subnet CIDR (e.g. 29)
    _IN amount          $int      (optional) Amount of returned Networks (e.g. 1)
    _RETURN             @HaCi::SOAP::Type::network
    _DOC                This Service returns all free Subnets of a certain size from networks, that fit the search criteria

addNet :
    _IN username      $string  Username
    _IN password      $string  Password
    _IN rootName      $string  Name of Root
    _IN ipaddress     $string  IP Address (E.g.: 192.168.0.1)
    _IN cidr          $int     CIDR (E.g.: 24)
    _IN description   $string  (optional) Description of the Network
    _IN state         $string  (optional) The State of the Network (One of: UNSPECIFIED, ALLOCATED PA, ALLOCATED PI, ALLOCATED UNSPECIFIED, SUB-ALLOCATED PA, LIR-PARTITIONED PA, LIR-PARTITIONED PI, EARLY-REGISTRATION, NOT-SET, ASSIGNED PA, ASSIGNED PI, ASSIGNED ANYCAST, ALLOCATED-BY-RIR, ALLOCATED-BY-LIR, ASSIGNED, RESERVED, LOCKED, FREE)
    _IN defSubnetSize $int     (optional) Default CIDR for Subnets 
    _IN templateName  $string  (optional) Template, which should be linked to the Network 
    _IN tags          $string	 (optional) Tags (seperate by comma)
    _RETURN           $string  Prints if addition has failed or was successfull
    _DOC              This function adds a Network

delNet :
    _IN username  $string  Username
    _IN password  $string  Password
    _IN root      $string  Root Name
    _IN network   $string  Network (e.g. 192.168.0.0/24)
    _RETURN       $string  Prints if deletion has failed or was successfull
    _DOC          This function deletes a Network
