--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: audit_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE audit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_id_seq OWNER TO "HaCi";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: audit; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE audit (
    id integer DEFAULT nextval('audit_id_seq'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    username character varying(100) DEFAULT ''::character varying NOT NULL,
    access_groups character varying(255) DEFAULT ''::character varying NOT NULL,
    action character varying(255) DEFAULT ''::character varying NOT NULL,
    object character varying(255) DEFAULT ''::character varying NOT NULL,
    value character varying(1024) DEFAULT ''::character varying NOT NULL,
    error character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.audit OWNER TO "HaCi";

--
-- Name: network_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_id_seq OWNER TO "HaCi";

--
-- Name: network; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network (
    id integer DEFAULT nextval('network_id_seq'::regclass) NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    network bigint DEFAULT (0)::bigint NOT NULL,
    description character varying(255) DEFAULT ''::character varying NOT NULL,
    state smallint DEFAULT (0)::smallint NOT NULL,
    def_subnet_size smallint DEFAULT (0)::smallint NOT NULL,
    tmpl_id integer DEFAULT 0 NOT NULL,
    ipv6_id bytea DEFAULT '\x'::bytea NOT NULL,
    create_from character varying(255) DEFAULT ''::character varying NOT NULL,
    create_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modify_from character varying(255) DEFAULT ''::character varying NOT NULL,
    modify_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    search_str character varying(255),
    CONSTRAINT network_def_subnet_size_check CHECK ((def_subnet_size >= 0))
);


ALTER TABLE public.network OWNER TO "HaCi";

--
-- Name: network_ac_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE network_ac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_ac_id_seq OWNER TO "HaCi";

--
-- Name: network_ac; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network_ac (
    id integer DEFAULT nextval('network_ac_id_seq'::regclass) NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    network bigint DEFAULT (0)::bigint NOT NULL,
    net_id integer DEFAULT 0 NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    acl integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.network_ac OWNER TO "HaCi";

--
-- Name: network_lock_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE network_lock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_lock_id_seq OWNER TO "HaCi";

--
-- Name: network_lock; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network_lock (
    id integer DEFAULT nextval('network_lock_id_seq'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    duration integer DEFAULT 0 NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    network_prefix numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    host_part numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    cidr smallint DEFAULT (0)::smallint NOT NULL,
    ipv6 smallint DEFAULT (0)::smallint NOT NULL
);


ALTER TABLE public.network_lock OWNER TO "HaCi";

--
-- Name: network_plugin_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE network_plugin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_plugin_id_seq OWNER TO "HaCi";

--
-- Name: network_plugin; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network_plugin (
    id integer DEFAULT nextval('network_plugin_id_seq'::regclass) NOT NULL,
    net_id integer DEFAULT 0 NOT NULL,
    plugin_id integer DEFAULT 0 NOT NULL,
    sequence integer DEFAULT 0 NOT NULL,
    new_line smallint DEFAULT (0)::smallint NOT NULL
);


ALTER TABLE public.network_plugin OWNER TO "HaCi";

--
-- Name: network_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE network_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_tag_id_seq OWNER TO "HaCi";

--
-- Name: network_tag; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network_tag (
    id integer DEFAULT nextval('network_tag_id_seq'::regclass) NOT NULL,
    net_id integer DEFAULT 0 NOT NULL,
    tag character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.network_tag OWNER TO "HaCi";

--
-- Name: network_v6; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE network_v6 (
    id bytea DEFAULT '\x'::bytea NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    network_prefix numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    host_part numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    cidr smallint DEFAULT (0)::smallint NOT NULL,
    CONSTRAINT network_v6_host_part_check CHECK ((host_part >= (0)::numeric)),
    CONSTRAINT network_v6_network_prefix_check CHECK ((network_prefix >= (0)::numeric))
);


ALTER TABLE public.network_v6 OWNER TO "HaCi";

--
-- Name: plugin_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE plugin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plugin_id_seq OWNER TO "HaCi";

--
-- Name: plugin; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE plugin (
    id integer DEFAULT nextval('plugin_id_seq'::regclass) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    filename character varying(255) DEFAULT ''::character varying NOT NULL,
    active smallint DEFAULT (0)::smallint NOT NULL,
    last_run timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    run_time integer DEFAULT 0 NOT NULL,
    last_error character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.plugin OWNER TO "HaCi";

--
-- Name: plugin_conf_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE plugin_conf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plugin_conf_id_seq OWNER TO "HaCi";

--
-- Name: plugin_conf; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE plugin_conf (
    id integer DEFAULT nextval('plugin_conf_id_seq'::regclass) NOT NULL,
    net_id integer NOT NULL,
    plugin_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.plugin_conf OWNER TO "HaCi";

--
-- Name: plugin_value_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE plugin_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plugin_value_id_seq OWNER TO "HaCi";

--
-- Name: plugin_value; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE plugin_value (
    id integer DEFAULT nextval('plugin_value_id_seq'::regclass) NOT NULL,
    net_id integer NOT NULL,
    plugin_id integer NOT NULL,
    origin integer NOT NULL,
    name character varying(255) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.plugin_value OWNER TO "HaCi";

--
-- Name: root_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE root_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.root_id_seq OWNER TO "HaCi";

--
-- Name: root; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE root (
    id integer DEFAULT nextval('root_id_seq'::regclass) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    description character varying(255) DEFAULT ''::character varying NOT NULL,
    ipv6 smallint DEFAULT (0)::smallint NOT NULL,
    create_from character varying(255) DEFAULT ''::character varying NOT NULL,
    create_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modify_from character varying(255) DEFAULT ''::character varying NOT NULL,
    modify_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.root OWNER TO "HaCi";

--
-- Name: root_ac_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE root_ac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.root_ac_id_seq OWNER TO "HaCi";

--
-- Name: root_ac; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE root_ac (
    id integer DEFAULT nextval('root_ac_id_seq'::regclass) NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    acl integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.root_ac OWNER TO "HaCi";

--
-- Name: setting_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setting_id_seq OWNER TO "HaCi";

--
-- Name: setting; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE setting (
    id integer DEFAULT nextval('setting_id_seq'::regclass) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    param character varying(255) DEFAULT ''::character varying NOT NULL,
    value character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.setting OWNER TO "HaCi";

--
-- Name: squad_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE squad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.squad_id_seq OWNER TO "HaCi";

--
-- Name: squad; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE squad (
    id integer DEFAULT nextval('squad_id_seq'::regclass) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    description character varying(255) DEFAULT ''::character varying NOT NULL,
    permissions integer DEFAULT 0 NOT NULL,
    create_from character varying(255) DEFAULT ''::character varying NOT NULL,
    create_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modify_from character varying(255) DEFAULT ''::character varying NOT NULL,
    modify_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.squad OWNER TO "HaCi";

--
-- Name: template_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_id_seq OWNER TO "HaCi";

--
-- Name: template; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE template (
    id integer DEFAULT nextval('template_id_seq'::regclass) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    type character varying(64) DEFAULT ''::character varying NOT NULL,
    create_from character varying(255) DEFAULT ''::character varying NOT NULL,
    create_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modify_from character varying(255) DEFAULT ''::character varying NOT NULL,
    modify_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.template OWNER TO "HaCi";

--
-- Name: template_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE template_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_entry_id_seq OWNER TO "HaCi";

--
-- Name: template_entry; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE template_entry (
    id integer DEFAULT nextval('template_entry_id_seq'::regclass) NOT NULL,
    tmpl_id integer DEFAULT 0 NOT NULL,
    type integer DEFAULT 0 NOT NULL,
    "position" integer DEFAULT 0 NOT NULL,
    description character varying(255) DEFAULT ''::character varying NOT NULL,
    size integer DEFAULT 1 NOT NULL,
    entries character varying(1023) DEFAULT ''::character varying NOT NULL,
    rows integer DEFAULT 1 NOT NULL,
    cols integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.template_entry OWNER TO "HaCi";

--
-- Name: template_value_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE template_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_value_id_seq OWNER TO "HaCi";

--
-- Name: template_value; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE template_value (
    id integer DEFAULT nextval('template_value_id_seq'::regclass) NOT NULL,
    tmpl_id integer DEFAULT 0 NOT NULL,
    tmpl_entry_id integer DEFAULT 0 NOT NULL,
    net_id integer DEFAULT 0 NOT NULL,
    value bytea NOT NULL
);


ALTER TABLE public.template_value OWNER TO "HaCi";

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: HaCi
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO "HaCi";

--
-- Name: user; Type: TABLE; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE TABLE "user" (
    id integer DEFAULT nextval('user_id_seq'::regclass) NOT NULL,
    username character varying(100) DEFAULT ''::character varying NOT NULL,
    password character varying(255) DEFAULT ''::character varying NOT NULL,
    group_ids character varying(255) DEFAULT ''::character varying NOT NULL,
    description character varying(255) DEFAULT ''::character varying NOT NULL,
    create_from character varying(255) DEFAULT ''::character varying NOT NULL,
    create_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modify_from character varying(255) DEFAULT ''::character varying NOT NULL,
    modify_date timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public."user" OWNER TO "HaCi";

--
-- Name: audit_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY audit
    ADD CONSTRAINT audit_pkey PRIMARY KEY (id);


--
-- Name: network_ac_net_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_ac
    ADD CONSTRAINT network_ac_net_id_key UNIQUE (net_id, group_id);


--
-- Name: network_ac_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_ac
    ADD CONSTRAINT network_ac_pkey PRIMARY KEY (id);


--
-- Name: network_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_lock
    ADD CONSTRAINT network_lock_pkey PRIMARY KEY (id);


--
-- Name: network_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_pkey PRIMARY KEY (id);


--
-- Name: network_plugin_net_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_plugin
    ADD CONSTRAINT network_plugin_net_id_key UNIQUE (net_id, plugin_id);


--
-- Name: network_plugin_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_plugin
    ADD CONSTRAINT network_plugin_pkey PRIMARY KEY (id);


--
-- Name: network_root_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_root_id_key UNIQUE (root_id, ipv6_id, network);


--
-- Name: network_tag_net_id_tag_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_tag
    ADD CONSTRAINT network_tag_net_id_tag_key UNIQUE (net_id, tag);


--
-- Name: network_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_tag
    ADD CONSTRAINT network_tag_pkey PRIMARY KEY (id);


--
-- Name: network_v6_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_v6
    ADD CONSTRAINT network_v6_pkey PRIMARY KEY (id, root_id);


--
-- Name: network_v6_root_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY network_v6
    ADD CONSTRAINT network_v6_root_id_key UNIQUE (root_id, network_prefix, host_part, cidr);


--
-- Name: plugin_conf_net_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin_conf
    ADD CONSTRAINT plugin_conf_net_id_key UNIQUE (net_id, plugin_id, name);


--
-- Name: plugin_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin_conf
    ADD CONSTRAINT plugin_conf_pkey PRIMARY KEY (id);


--
-- Name: plugin_name_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin
    ADD CONSTRAINT plugin_name_key UNIQUE (name);


--
-- Name: plugin_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin
    ADD CONSTRAINT plugin_pkey PRIMARY KEY (id);


--
-- Name: plugin_value_net_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin_value
    ADD CONSTRAINT plugin_value_net_id_key UNIQUE (net_id, plugin_id, name);


--
-- Name: plugin_value_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY plugin_value
    ADD CONSTRAINT plugin_value_pkey PRIMARY KEY (id);


--
-- Name: root_ac_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY root_ac
    ADD CONSTRAINT root_ac_pkey PRIMARY KEY (id);


--
-- Name: root_ac_root_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY root_ac
    ADD CONSTRAINT root_ac_root_id_key UNIQUE (root_id, group_id);


--
-- Name: root_name_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY root
    ADD CONSTRAINT root_name_key UNIQUE (name);


--
-- Name: root_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY root
    ADD CONSTRAINT root_pkey PRIMARY KEY (id);


--
-- Name: setting_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY setting
    ADD CONSTRAINT setting_pkey PRIMARY KEY (id);


--
-- Name: setting_user_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY setting
    ADD CONSTRAINT setting_user_id_key UNIQUE (user_id, param, value);


--
-- Name: squad_name_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY squad
    ADD CONSTRAINT squad_name_key UNIQUE (name);


--
-- Name: squad_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY squad
    ADD CONSTRAINT squad_pkey PRIMARY KEY (id);


--
-- Name: template_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY template_entry
    ADD CONSTRAINT template_entry_pkey PRIMARY KEY (id);


--
-- Name: template_name_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY template
    ADD CONSTRAINT template_name_key UNIQUE (name);


--
-- Name: template_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY template
    ADD CONSTRAINT template_pkey PRIMARY KEY (id);


--
-- Name: template_value_net_id_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY template_value
    ADD CONSTRAINT template_value_net_id_key UNIQUE (net_id, tmpl_id, tmpl_entry_id);


--
-- Name: template_value_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY template_value
    ADD CONSTRAINT template_value_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: HaCi; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: network_description_idx; Type: INDEX; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE INDEX network_description_idx ON network USING btree (description);


--
-- Name: network_lock_uniq_network; Type: INDEX; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE UNIQUE INDEX network_lock_uniq_network ON network_lock USING btree (root_id, network_prefix, host_part, cidr, ipv6);


--
-- Name: network_network_idx; Type: INDEX; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE INDEX network_network_idx ON network USING btree (network);


--
-- Name: network_search_str_idx; Type: INDEX; Schema: public; Owner: HaCi; Tablespace: 
--

CREATE INDEX network_search_str_idx ON network USING btree (search_str);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

