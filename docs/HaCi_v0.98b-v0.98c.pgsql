ALTER TABLE network 
	ADD COLUMN search_str character varying(255) DEFAULT ''::character varying;

CREATE INDEX network_description_idx ON network USING btree (description);
CREATE INDEX network_search_str_idx ON network USING btree (search_str);
CREATE SEQUENCE network_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.network_tag_id_seq OWNER TO "HaCi2";

CREATE TABLE network_tag (
    id integer DEFAULT nextval('network_tag_id_seq'::regclass) NOT NULL,
    net_id integer DEFAULT 0 NOT NULL,
    tag character varying(255) DEFAULT ''::character varying NOT NULL
);

ALTER TABLE public.network_tag OWNER TO "HaCi2";

ALTER TABLE ONLY network_tag
    ADD CONSTRAINT network_tag_pkey PRIMARY KEY (id);

ALTER TABLE ONLY network_tag
    ADD CONSTRAINT network_tag_net_id_tag_key UNIQUE (net_id, tag);
