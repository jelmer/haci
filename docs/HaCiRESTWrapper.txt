REST wrapper for HaCi XML-RPC-API

The methods and parameters are equal to the XML-RPC-API.
The parameters have to be URL encoded (i.e: '/' => '%2f')

Authentication: 
 * via HTTP basic authentication
 * via username and password parameter

USAGE: 
 * http(s)://<hostname>/RESTWrapper/[API-METHOD]?username=<user>&password=<password>&[API-PARAMETER]
 * http(s)://<user>@<pass>@<hostname>/RESTWrapper/[API-METHOD]?[API-PARAMETER]

Examples:
  * http(s)://foo:bar@<hostname>/RESTWrapper/search?search=test?rootName=bigRoot
  * http(s)://<hostname>/RESTWrapper/getFreeSubnets?username=foo&password=bar&rootName=bigRoot&supernet=192.168.0.0%2f24&cidr=32&amount=3

	$ curl 'http://demo.haci.larsux.de/RESTWrapper/getFreeSubnets?username=admin&password=admin&rootName=testRoot&supernet=192.168.0.0%2f24&cidr=32&amount=3'
	["192.168.0.1/32","192.168.0.2/32","192.168.0.3/32"]

Return: JSON-Object
