
ALTER TABLE template_entry
	ALTER COLUMN entries TYPE character varying(1023) /* TYPE change - table: template_entry original: character varying(255) new: character varying(1023) */;

CREATE SEQUENCE audit_id_seq
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

ALTER TABLE public.audit_id_seq OWNER TO "HaCi";

CREATE TABLE audit (
	id integer DEFAULT nextval('audit_id_seq'::regclass) NOT NULL,
	ts timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
	username character varying(100) DEFAULT ''::character varying NOT NULL,
	access_groups character varying(255) DEFAULT ''::character varying NOT NULL,
	action character varying(255) DEFAULT ''::character varying NOT NULL,
	object character varying(255) DEFAULT ''::character varying NOT NULL,
	value character varying(1024) DEFAULT ''::character varying NOT NULL,
	error character varying(255) DEFAULT ''::character varying NOT NULL
);

ALTER TABLE audit
	ADD CONSTRAINT audit_pkey PRIMARY KEY (id);

ALTER TABLE public."audit" OWNER TO "HaCi";
CREATE SEQUENCE network_lock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.network_lock_id_seq OWNER TO "HaCi";

CREATE TABLE network_lock (
    id integer DEFAULT nextval('network_lock_id_seq'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone NOT NULL,
    duration integer DEFAULT 0 NOT NULL,
    root_id integer DEFAULT 0 NOT NULL,
    network_prefix numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    host_part numeric(20,0) DEFAULT (0)::numeric NOT NULL,
    cidr smallint DEFAULT (0)::smallint NOT NULL,
    ipv6 smallint DEFAULT (0)::smallint NOT NULL
);

ALTER TABLE public.network_lock OWNER TO "HaCi";

ALTER TABLE ONLY network_lock
    ADD CONSTRAINT network_lock_pkey PRIMARY KEY (id);

CREATE UNIQUE INDEX network_lock_uniq_network ON network_lock USING btree (root_id, network_prefix, host_part, cidr, ipv6);

