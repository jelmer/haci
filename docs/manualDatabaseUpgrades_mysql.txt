# These are the database scheme changes between different HaCi versions. New tables were created by HaCi automatically.
# Also those changes where automatically performed by HaCi. 
# Since version 0.97 there's a config option 'autoUpgradeDatabase', which gives you the opportunity to enable resp. disable the automatic upgrade function.


0.75 -> 0.8:
ALTER TABLE network DROP INDEX network;
ALTER TABLE network ADD COLUMN ipv6ID varchar(22) NOT NULL DEFAULT '';
ALTER TABLE network ADD UNIQUE  (network, rootID, ipv6ID);

ALTER TABLE root ADD COLUMN ipv6 tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE networkAC DROP INDEX rootID;
ALTER TABLE networkAC ADD COLUMN netID integer(11) NOT NULL DEFAULT '0';
ALTER TABLE networkAC ADD UNIQUE  (netID, groupID);


0.8 -> 0.91
ALTER TABLE plugin DROP COLUMN date;
ALTER TABLE plugin ADD COLUMN runTime integer(11) NOT NULL DEFAULT '0';
ALTER TABLE plugin ADD COLUMN lastError varchar(255) NOT NULL DEFAULT '';

ALTER TABLE networkPlugin ADD COLUMN sequence integer(11) NOT NULL DEFAULT '0';
ALTER TABLE networkPlugin ADD COLUMN newLine tinyint(4) NOT NULL DEFAULT '0';


0.91 -> 0.95
ALTER TABLE plugin ADD COLUMN filename varchar(255) NOT NULL DEFAULT '';

ALTER TABLE network ADD COLUMN defSubnetSize TINYINT(4) unsigned NOT NULL DEFAULT '0';
ALTER TABLE network ADD INDEX  (network);


0.95 -> 0.96
ALTER TABLE network CHANGE COLUMN ipv6ID ipv6ID varbinary(22) NOT NULL DEFAULT '';

ALTER TABLE networkV6 CHANGE COLUMN ID ID varbinary(22) NOT NULL DEFAULT '';

0.96 -> 0.97
ALTER TABLE networkV6 CHANGE COLUMN networkPrefix networkPrefix bigint(20) unsigned NOT NULL DEFAULT '0';
ALTER TABLE networkV6 CHANGE COLUMN hostPart hostPart bigint(20) unsigned NOT NULL DEFAULT '0';

0.97 -> 0.98
ALTER TABLE templateEntry CHANGE COLUMN entries entries text NOT NULL;
CREATE TABLE `audit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ts` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(255) NOT NULL DEFAULT '',
  `accessGroups` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `object` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `error` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

0.98 -> 0.98b
CREATE TABLE `networkLock` (
  `ID` int(11) NOT NULL auto_increment,
  `ts` datetime NOT NULL default '0000-00-00 00:00:00',
  `duration` int(11) NOT NULL default '0',
  `rootID` int(11) NOT NULL default '0',
  `networkPrefix` bigint(20) unsigned NOT NULL default '0',
  `hostPart` bigint(20) unsigned NOT NULL default '0',
  `cidr` smallint(6) NOT NULL default '0',
  `ipv6` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `rootID` (`rootID`,`networkPrefix`,`hostPart`,`cidr`,`ipv6`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

0.98b -> 0.98c
ALTER TABLE network ADD COLUMN searchStr varchar(255) DEFAULT '';
ALTER TABLE network CHANGE COLUMN network network bigint(20) unsigned NOT NULL DEFAULT 0;
ALTER TABLE network ADD INDEX  (description);
ALTER TABLE network ADD INDEX  (searchStr);
CREATE TABLE `networkTag` (
  `ID` int(11) NOT NULL auto_increment,
  `netID` int(11) NOT NULL default '0',
  `tag` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `netID` (`netID`,`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
