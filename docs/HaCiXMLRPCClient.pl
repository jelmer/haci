#!/usr/bin/perl

# Demo script using all available functions of the HaCi XMPRPC-API

use strict;
use warnings;
use Data::Dumper;
use Frontier::Client;

#my $server	= 'demo.haci.larsux.de';
my $server	= 'hacidev.larsux.de';
my $user		= 'admin';
my $pass		= 'admin';
my $a				= $ARGV[0] || 0;

my $api			= new Frontier::Client(url => "http://$server/RPC2");
my $session	= $api->call('login', [$user, $pass]);
die 'Login failed!' unless $session;

$a	= 999 if $a && $a !~ /^\d+$/;

if (!$a) {
	print "USAGE $0 Number

Options:
	Number:
		1: search
		2: getFreeSubnets
		3: getFreeSubnetsFromSearch
		4: listRoots
		5: addRoot
		6: editRoot
		7: delRoot
		8: addNet
		9: editNet
	 10: delNet
	 11: assignFreeSubnet
	 12: getNetworkDetails
	 13: getSubnets


";
	exit 0;
}
elsif ($a == 1) {
	print Dumper($api->call('search', $session, 'test', 0, 0, 'Pool', {'Pool-Typ' => 'DSL'}, 'testRoot', 1, 1));
}
elsif ($a == 2) {
	print Dumper($api->call('getFreeSubnets', $session, 'testRoot', '192.168.0.0/24', 29, 10));
}
elsif ($a == 3) {
	print Dumper($api->call('getFreeSubnetsFromSearch', $session, 'HaCiAPI', 0, 0, 'Pool', {'Pool-Typ' => 'DSL'}, 'testRoot', 29, 10));
}
elsif ($a == 4) {
	print Dumper($api->call('listRoots', $session));
}
elsif ($a == 5) {
	print Dumper($api->call('addRoot', $session, 'testRoot', 'Test Root', 0));
}
elsif ($a == 6) {
	print Dumper($api->call('editRoot', $session, 'testRoot', 'Testroot', 'tEST rOOT'));
}
elsif ($a == 7) {
	print Dumper($api->call('delRoot', $session, 'Testroot'));
}
elsif ($a == 8) {
	print Dumper($api->call('addNet', $session, 'testRoot', '192.168.0.128/25', 'HaCiAPI Test Network', 'ASSIGNED PI', 29, 'Pool', {'Pool-Typ' => 'DSL', Name => 'TEST'}));
}
elsif ($a == 9) {
	print Dumper($api->call('editNet', $session, 'testRoot', '192.168.0.128/25', {description => 'HaCiAPI Test Network modified', state => 'FREE', defSubnetSize => 27}));
}
elsif ($a == 10) {
	print Dumper($api->call('delNet', $session, 'testRoot', '192.168.0.128/25'));
}
elsif ($a == 11) {
	print Dumper($api->call('assignFreeSubnet', $session, 'testRoot', '192.168.0.128/25', 29, 'test29er under testRoot', '', 32, 'Pool', {'Pool-Typ' => 'DSL', Name => 'TEST'}));
}
elsif ($a == 12) {
	print Dumper($api->call('getNetworkDetails', $session, 'testRoot', '192.168.0.128/25'));
}
elsif ($a == 13) {
	print Dumper($api->call('getSubnets', $session, 'testRoot', '192.168.0.128/25'));
} else {
	my $method	= shift @ARGV;
	print Dumper($api->call($method, $session, @ARGV));
}

exit 0;
