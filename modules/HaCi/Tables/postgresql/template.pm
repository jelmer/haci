package HaCi::Tables::postgresql::template;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'template'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		name					=> 'name',
		type					=> 'type',
		createFrom		=> 'create_from',
		createDate		=> 'create_date',
		modifyFrom		=> 'modify_from',
		modifyDate		=> 'modify_date',
	}
}

1;

# vim:ts=2:sw=2:sws=2
