package HaCi::Tables::postgresql::plugin;
use base 'DBIEasy::postgresql';
   
sub TABLE { #Table Name
	'plugin'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		name					=> 'name',
		filename			=> 'filename',
		active				=> 'active',
		lastRun				=> 'last_run',
		runTime				=> 'run_time',
		lastError			=> 'last_error',
	}
}

1;

# vim:ts=2:sw=2:sws=2
