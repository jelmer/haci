package HaCi::Tables::postgresql::templateEntry;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'template_entry'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		tmplID				=> 'tmpl_id',
		type					=> 'type',
		position			=> 'position',
		description		=> 'description',
		size					=> 'size',
		entries				=> 'entries',
		rows					=> 'rows',
		cols					=> 'cols',
	}
}

1;

# vim:ts=2:sw=2:sws=2
