package HaCi::Tables::postgresql::audit;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'audit'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		ts						=> 'ts',
		username			=> 'username',
		accessGroups	=> 'access_groups',
		action				=> 'action',
		object				=> 'object',
		value					=> 'value',
		error					=> 'error',
	}
}

1;

# vim:ts=2:sw=2:sws=2
