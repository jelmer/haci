package HaCi::Tables::postgresql::rootAC;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'root_ac'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		rootID				=> 'root_id',
		groupID				=> 'group_id',
		ACL						=> 'acl',
	}
}

1;

# vim:ts=2:sw=2:sws=2
