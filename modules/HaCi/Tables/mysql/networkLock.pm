package HaCi::Tables::mysql::networkLock;
use base 'DBIEasy::mysql';

sub TABLE { #Table Name
	'networkLock'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	1
}

sub CREATETABLE { # Table Create Definition
	q{
		`ID` integer NOT NULL auto_increment,
    `ts` datetime NOT NULL default '0000-00-00 00:00:00',
    `duration` integer NOT NULL default '0',
    `rootID` int NOT NULL default 0,
    `networkPrefix` bigint UNSIGNED NOT NULL default 0,
    `hostPart` bigint UNSIGNED NOT NULL default 0,
    `cidr` smallint NOT NULL default 0,
    `ipv6` smallint NOT NULL default 0,
		PRIMARY KEY  (`ID`),
		UNIQUE KEY (`rootID`, `networkPrefix`,`hostPart`,`cidr`, `ipv6`)
	}
}

sub _log {
	my $self	= shift;
	my @msg		= @_;
	
	DBIEasy::mysql::_log($self, @msg);
}

sub _carp {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_carp($self, $message, %info);
}

sub _croak {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_croak($self, $message, %info);
}

1;

# vim:ts=2:sw=2:sws=2
