package HaCi::Tables::mysql::audit;
use base 'DBIEasy::mysql';

sub TABLE { #Table Name
	'audit'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	1
}

sub CREATETABLE { # Table Create Definition
  q{
  `ID` int(11) NOT NULL auto_increment,
  `ts` datetime NOT NULL default '0000-00-00 00:00:00',
  `username` varchar(255) NOT NULL default '',
  `accessGroups` varchar(255) NOT NULL default '',
  `action` varchar(255) NOT NULL default '',
  `object` varchar(255) NOT NULL default '',
  `value` text NOT NULL,
  `error` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`ID`)
  }
}

sub _log {
	my $self	= shift;
	my @msg		= @_;
	
	DBIEasy::mysql::_log($self, @msg);
}

sub _carp {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_carp($self, $message, %info);
}

sub _croak {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_croak($self, $message, %info);
}
1;
