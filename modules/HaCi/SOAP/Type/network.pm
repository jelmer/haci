package HaCi::SOAP::Type::network;

=begin WSDL

	_ATTR rootName		$string Name of Root
	_ATTR network			$string Network
	_ATTR description	$string Description
	_ATTR state				$string State
	_ATTR tags				$string Tags

=cut

sub new {
	my $class				= shift;
	my $rootName		= shift || '';
	my $network			= shift || '';
	my $description	= shift || '';
	my $state				= SOAP::Data->type(string => (shift || 0));
	my $tags				= shift || '';

	bless {
		rootName		=> $rootName,
		network			=> $network,
		description	=> $description,
		state				=> $state,
		tags				=> $tags,
	}, $class;
}

1;
