#!/usr/bin/perl

@mands	= qw/CGI CGI::Ajax CGI::Carp CGI::Cookie CGI::Session Class::Accessor Class::MakeMethods Config::General DBD::mysql Digest::MD5 Digest::SHA Encode Encode::Guess File::Temp HTML::Entities Locale::gettext Log::LogLite Math::Base85 Math::BigInt#1.87 Net::CIDR Net::IMAP::Simple Net::IPv6Addr Net::SNMP Storable Template Time::Local/;
my
@opts	= qw/Cache::FastMmap Cache::FileCache DNS::ZoneParse IO::Socket::INET6 Math::BigInt::GMP Net::DNS Net::Nslookup Net::Ping Pod::WSDL SOAP::Transport::HTTP SQL::Translator#0.09000 SQL::Translator::Diff Text::CSV_XS Apache::DBI/;

for(@mands){
	tr/A-Z/a-z/;
	s/::/-/g;
	s/^/\tlib/;
	s/$/-perl,/;
	print $_;
	print "\n"
}
print "---\n";
for(@opts){
	tr/A-Z/a-z/;
	s/::/-/g;
	s/^/\tlib/;
	s/$/-perl,/;
	print $_;
	print "\n"
}

